package imt.btree;

import java.util.ArrayList;
import java.util.Comparator;

// Corentin Le Berre et Benjamin Boboul

public class Tree {

    private static int order;

    public static void setOrder(int _order) {
        order = _order;
    }

    private Tree root;
    private Integer value;
    private ArrayList<Tree> childs;

    public Tree getRoot() {
        return this.root;
    }

    public void setRoot(Tree root) {
        this.root = root;
    }

    public Integer getValue() {
        return this.value;
    }

    // complexitée : O(1)
    public void setValue(Integer value) {
        this.value = value;
    }

    // complexitée : O(exp(n))
    private void afficher(int shift) {
        StringBuilder format = new StringBuilder();
        for (int i = 0 ; i < shift ; i++) format.append('\t');
        format.append(getRoot() == null ? "|-(Root)" : "/"+(getRoot().getValue() == null ? "~" : getRoot().getValue())+"|>"+this.value+"").append('\n');
        System.out.println(format);
        for (Tree child : childs) child.afficher(shift+1);
    }

    public void afficher() {
        afficher(0);
    }

    // complexitée : O(a) Constante
    public Tree() { // Root
        setRoot(null);
        setValue(null);
        childs = new ArrayList<>();
    }

    // complexitée : O(a) Constante
    public Tree(Tree root, Integer value) {
        setRoot(root);
        setValue(value);
        childs = new ArrayList<>();
    }

    // complexitée : O(n) linéaire
    private Integer indexOfMedian() {
        return (int) Math.ceil(childs.size()/2.0);
    }

    // complexitée : O(log(n)) logarithmique
    protected void equilibrie() {
        if (childs.size() == order+1) {
            //System.out.println(childs.get(indexOfMedian()).getValue());
            ArrayList<Tree> below = new ArrayList<>(childs.subList(1, indexOfMedian()));
            ArrayList<Tree> median = new ArrayList<>(childs.subList(indexOfMedian(), indexOfMedian()+1));
            ArrayList<Tree> above = new ArrayList<>(childs.subList(indexOfMedian()+1, childs.size()));
            if (getRoot()!=null) {
                childs.removeAll(median);
                childs.removeAll(above);
                for (Tree child : above) {
                    median.get(0).addEntry(child);
                }
                getRoot().addEntry(median.get(0), true);
            } else {
                childs.removeAll(below);
                for (Tree child : below) {
                    //System.out.println(child.getValue());
                    childs.get(0).addEntry(child);
                }
                childs.removeAll(above);
                for (Tree child : above) {
                    median.get(0).addEntry(child);
                }
            }
        }
        childs.sort(Comparator.comparing(Tree::getValue, Comparator.nullsFirst(Comparator.naturalOrder())));
    }

    // complexitée : O(log(n)) logarithmique
    public void addEntry(Tree node) {
        if (this.childs.isEmpty()) this.childs.add(new Tree(this, null));
        this.childs.add(node);
        this.childs.get(this.childs.indexOf(node)).setRoot(this);
        equilibrie();
    }

    // complexitée : O(log(n)) logarithmique
    public void addEntry(int value) {
        addEntry(value, false);
    }

    // complexitée : O(log(n)) logarithmique
    public void addEntry(Tree value, boolean locked) {
        if(childs.isEmpty() && !locked) {
            addEntry(value);
        } else if (childs.stream().allMatch(child -> child.childs.isEmpty()) && !locked) {
            addEntry(value);
        } else if (locked) {
            addEntry(value);
        } else {childs.get(indexOfNext(value.getValue())).addEntry(value);}
    }

    public void addEntry(int value, boolean locked) {
        addEntry(new Tree(this, value), locked);
    }

    // complexitée : O(a) linéaire
    public int indexOfNext(int value) {
        int res = 0;
        for(Tree child : childs) {
            if ((child.getValue() == null) || (child.getValue() < value)) res = childs.indexOf(child);
        }
        return res;
    }

    // complexitée : O(log(n)) logarithmique
    public boolean isPresent(int value) {
        boolean result = false;
        for (Tree child : childs) {
            if ((child.getValue() == null) || child.getValue() == value) result = true;
            if (result) break;
        }
        if (!result) result = childs.get(indexOfNext(value)).isPresent(value);
        return result;
    }

    // complexitée : O(log(n)) logarithmique
    public boolean remove(int value) {
        boolean result = false;
        if (!childs.isEmpty()) {
            for (Tree child : childs) {
                if (child.getValue() != null && child.getValue() == value) {
                    if (!child.childs.isEmpty()) {
                        ArrayList<Tree> links = new ArrayList<>();
                        links.add(child.childs.get(1));
                        child.setValue(links.get(0).getValue());
                        result = childs.remove(child);
                    } else {
                        childs.remove(child);
                    }
                }
                if (result) break;
            }
            if (!result) {
                result = childs.get(indexOfNext(value)).remove(value);
            }
        }
        return result;
    }
}
