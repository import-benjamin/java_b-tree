# Conception et réalisation d'un arbre-B (B-tree)

## Préparation

Avant de se lancer dans la réalisation et le développement des algorithmes du projet, il est nécessaire
de prendre le temps de le comprendre et de le dérouler sur des données tests. Ensuite, organiser le
projet en classes et d’expliciter les liens entre les classes.

## Réalisation algorithmique / langage Java

Il s'agit de l'étape de développement algorithmique du projet, suivi de la programmation en Java.
Durant cette étape, plusieurs tâches seront réalisées :

1. Spécification des signatures des différentes méthodes nécessaires au déroulement du projet
2. Réalisation des algorithmes des différentes méthodes
3. Estimation de la complexité temporelle des différents algorithmes

## Documentation

Une documentation doit nécessairement accompagner les codes Java, lors de la livraison. La
documentation est une facette essentielle du projet car elle constitue le référentiel de base concernant
le projet après réalisation. La documentation ne doit pas dépasser 4 pages.

## Validation

Lorsque le programme est réalisé, il convient de s'assurer qu'il répond au cahier des charges. On parle
de tests unitaires pour désigner la validation de la conformité du projet à chacune des clauses du cahier
des charges.

## Remarques générales

Le projet peut être réalisé par une équipe, composée d’une personne, 2 personnes.

## Projet par défaut

Il est possible de choisir un sujet de projet différent de celui qui est présenté ci-dessous (arbre-B). Le
sujet en question doit être proposé et validé par les deux enseignants responsables de l’UV, avant le
30 novembre 2017.

## Introduction du sujet

Un arbre B est une structure d’arbre N-aire équilibré.
Chaque sommet de l’arbre est composé d’au moins `M` couples (`Clé`, `Pointeur`) et au maximum `2*M` couples (`Clé`, `Pointeur`). `M` est un paramètre statique de l’arbre.
Clé est une valeur entière et Pointeur est le lien vers un sommet fils. Les clés sont triées 
Ainsi un sommet est composé de : `Pointeur0`, (`Clé1`, `Pointeur1`), (`Clé2`, `Pointeur2`), ... (`CléK`, `PointeurK`).

```math
M <= K <= 2M
```

Ainsi, `PointeurI`, tel que $`1 <= I < K`$, est le lien vers `le sommet fils` dont les clés sont comprises entre `CléI` et `CléI+1`.

Par exemple :

- __`Pointeur1` est le lien vers `le sommet fils` dont les clés sont comprises entre `Clé1` et `Clé2`__.
- __`Pointeur0` est le lien vers `le sommet fils` dont les clés sont inférieures à `Clé1`__.
- __`PointeurK` est le lien vers `le sommet fils` dont les clés sont supérieures à `CléK`__.

Trois fonctionnalités de base caractérisent le fonctionnement de l’arbre-B : recherche (clé), insertion (clé) et suppression (clé). Ces méthodes sont détaillées dans le lien ci-dessous.

https://fr.wikipedia.org/wiki/Arbre_B. __D’autres sites web présentent également le fonctionnement de
cette structure de données.__


### Question 1 : questions générales (3 points)

1. Donnez en quelques lignes, l’historique de l’arbre B. Et quelle est la signification de la lettre
_« B »_.

> Le créateur des arbres B, Rudolf Bayer, n'a pas explicité la signification du « B ». L'explication la plus fréquente est que le B correspond au terme anglais « balanced » (en français : « équilibré »). Cependant, il pourrait aussi découler de « Bayer », du nom du créateur, ou de « Boeing », du nom de la firme pour laquelle le créateur travaillait (Boeing Scientific Research Labs). 

2. Est-ce que l’arbre-B est un arbre binaire ?

>  De plus un B-arbre grandit à partir de la racine, contrairement à un arbre binaire de recherche qui croît à partir des feuilles. 

Un arbre-B n'est pas un [arbre binaire](https://fr.wikipedia.org/wiki/Arbre_binaire_de_recherche).

3. Est-ce que l’arbre-B est un arbre équilibré ?

> De plus, la construction des arbres B garantit qu’__un arbre B est toujours équilibré__. Chaque clé d’un nœud interne est en fait une borne qui distingue les sous-arbres de ce nœud.

4. Est-ce que l’arbre B grandit à partir de la racine ou à partir des feuilles ?

> De plus __un B-arbre grandit à partir de la racine__, contrairement à un arbre binaire de recherche qui croît à partir des feuilles. 

5. A quoi servent les arbres B ?

> Les arbres B sont principalement mis en œuvre dans les mécanismes de gestion de bases de données et de systèmes de fichiers. __Ils stockent les données sous une forme triée et permettent une exécution des opérations d'insertion et de suppression en temps toujours logarithmique__.

6. Quelle est la différence entre l’arbre B et l’arbre B+ ?

> L’arbre B+ diffère légèrement de l’arbre B, en ceci que toutes les données sont stockées exclusivement dans des feuilles, et celles-ci sont reliées entre elles. 

### Question 2 : Réalisation de l’algorithme et codage en Java (14 points)

1. Réalisez les algorithmes des fonctions de recherche (6 points), d’insertion (4 points) et de
suppression __(4 points)__
1. Evaluer leurs complexités
1. Réalisez en Java les différentes fonctions
1. Réalisez des tests avec L = 2, L=3, L=5, L=10. Pour chacun de ces cas de figures, considérez 2
et 3 niveaux.

> _Réalisez dans l’ordre les fonctions recherche (algorithme, analyse de la complexité et
codage Java), suivi des fonctions d’insertion et de suppression_

### Question 3 : réalisez la version arbre-B+ (3 points)

## Vidéos et documentations el ligne

Il existe de nombreuses vidéos publiques sur le fonctionnement de l’arbre B. Parmi lesquelles, citons :

- [youtube : B-tree example](https://www.youtube.com/watch?v=coRJrcIYbF4)
- [youtube : B+ Tree basics 1](https://www.youtube.com/watch?v=CYKRMz8yzVU)
- [canal-u.tv : Arbre B](https://www.canal-u.tv/video/inria/arbre_b.22441)